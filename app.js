import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import postRoutes from './routes/posts.js';
import userRoutes from './routes/user.js';

dotenv.config();
const app = express();

mongoose
  .connect(process.env.DB_URL)
  .then(() => {
    console.info('Connected to database!');
  })
  .catch(() => {
    console.info('Connection to database failed!');
  });

const port = process.env.PORT || 8000;

app.use(cors());
app.use(express.json());

app.use(express.static('public'));
app.use('/images', express.static('images'));

app.use('/api/posts', postRoutes);

app.use('/api/user', userRoutes);

app.listen(port, () => console.info('server listening on port ', port));
