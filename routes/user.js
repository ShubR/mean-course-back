import express from "express";
import User from "../models/user.js";
import bcrypt, { hash } from "bcrypt";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";

dotenv.config();

const router = express.Router();

router.post("/signup", (req, res) => {
  bcrypt.hash(req.body.password, 10).then((hash) => {
    const user = new User({
      email: req.body.email,
      password: hash,
    });

    user
      .save()
      .then((result) => {
        res
          .status(201)
          .json({ message: "User signedup successfully!", result: result });
      })
      .catch((err) => {
        res
          .status(500)
          .json({ message: "An error occured while signing up!", error: err });
      });
  });
});

router.post("/login", (req, res) => {
  let fetchedUser;
  User.findOne({ email: req.body.email })
    .then((user) => {
      if (!user) {
        return res.send(401).json({ message: "Auth failed!" });
      }

      fetchedUser = user;

      return bcrypt.compare(req.body.password, user.password);
    })
    .then((result) => {
      if (!result) {
        return res.send(401).json({ message: "Auth failed!" });
      }

      const token = jwt.sign(
        { email: fetchedUser.email, userId: fetchedUser._id },
        process.env.JWT_SECRET_KEY,
        { expiresIn: "1h" }
      );

      res
        .status(200)
        .json({ token: token, expiresIn: 3600, userId: fetchedUser._id });
    })
    .catch((err) => {
      return res.send(401).json({ message: "Auth failed!" });
    });
});

export default router;
