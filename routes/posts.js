import express from 'express';
import Post from '../models/post.js';
import multer from 'multer';

import checkAuth from '../middleware/check-auth.js';

const router = express.Router();

const MIME_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg',
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];

    let error = new Error('Invalid mime type');
    if (isValid) {
      error = null;
    }
    cb(error, 'images');
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLocaleLowerCase().split(' ').join('-');
    const ext = MIME_TYPE_MAP[file.mimetype];
    cb(null, `${name}-${Date.now()}.${ext}`);
  },
});

const upload = multer({ storage: storage });

router.post('', checkAuth, upload.single('image'), (req, res) => {
  req.body.imagePath = `${req.protocol}://${req.get('host')}/images/${
    req.file.filename
  }`;
  req.body.creator = req.userData.userId;
  new Post(req.body).save().then((createdPost) => {
    res
      .status(201)
      .json({ message: 'Post added successfully!', post: createdPost });
  });
});

router.get('', (req, res) => {
  const pageSize = +req.query.pagesize;
  const currentPage = +req.query.page;

  const postQuery = Post.find();

  let fetchedPosts;

  if (pageSize && currentPage) {
    postQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }

  postQuery
    .then((documents) => {
      fetchedPosts = documents;
      return Post.count();
    })
    .then((count) => {
      res.status(200).json({
        message:
          fetchedPosts.length > 0
            ? 'Posts fetched successfully !'
            : 'Post list is empty!',
        posts: fetchedPosts,
        maxPosts: count,
      });
    });
});

router.delete('/:id', checkAuth, (req, res) => {
  Post.deleteOne({ _id: req.params.id, creator: req.userData.userId }).then(
    (result) => {
      if (result.deletedCount) {
        res.status(200).json({ message: 'Deleted sucessfully!' });
      } else {
        res.status(401).json({ message: 'Not authorized!' });
      }
    }
  );
});

router.get('/:id', (req, res) => {
  Post.findById(req.params.id, (error, post) => {
    if (error) {
      res.status(201).json({ message: 'Post not found', error: error });
    }
    res.status(200).json({ message: 'Post fetched successfully!', post: post });
  });
});

router.put('/:id', checkAuth, upload.single('image'), (req, res) => {
  if (req.file) {
    req.body.imagePath = `${req.protocol}://${req.get('host')}/images/${
      req.file.filename
    }`;
  }

  Post.updateOne(
    { _id: req.params.id, creator: req.userData.userId },
    req.body
  ).then((result) => {
    if (result.modifiedCount) {
      res.status(200).json({ message: 'Updated sucessfully!' });
    } else {
      res.status(401).json({ message: 'Not authorized!' });
    }
  });
});

export default router;
